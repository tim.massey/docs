# Authentication

The API uses [JSON Web Tokens](https://jwt.io/) for authentication where necessary.
For the rest of this documentation we refer to these as JWT's.
To obtain a JWT use the `/authenticate` endpoint.

Tokens are issued for a lifetime of 15 minutes which should be enough to undertake any action via the API.
We may implement long lived tokens based on feedback, but as this is carries an inherent security risk, we require
re-authentication with each set of actions.

API requests expect for the API token to be included in all API requests to the server in an `X-Auth-Token` header as follows:

`X-Auth-Token: <your_jwt>`

<aside class="notice">
You must replace <code>your_jwt</code> with your personal API key.
</aside>

## POST /authenticate

To obtain a JWT you need to POST your username and password to this endpoint with the structure as outlined here.

The JWT is valid for a period of 15 minutes from the time of issue which should be sufficient to undertake any operations
using the API while minimising security risks.

Note: the username and password belong to the store owner. There is currently a limitation of one user account per store.

```json
{
  "username": "<your_username>",
  "password": "<your_password>"
}
```
> If the supplied credentials are valid the above command returns a JSON response with the JWT as outlined below:

```json
{
  "person": {
    "id": "<your_id>",
    "role": "<your_role>"
  },
  "token": "<your_jwt>"
}
```


## GET /info

Once you have a JWT you can check its contents using the `/info` endpoint. It will return the details as outlined here:

```json
{
    "user_id": 111,
    "seller_account_id": 222,
    "created": "<created_date>",
    "expires": "<expiry_date>",
    "roles": [
        "<your_role>",
        "<your_other_role>"
    ]
}
```