# General Usage

## GET /info

Once you have a JWT you can check its contents using the `/info` endpoint. It will return the details as outlined here:

```json
{
    "user_id": 111,
    "seller_account_id": 222,
    "created": "<created_date>",
    "expires": "<expiry_date>",
    "roles": [
        "<your_role>",
        "<your_other_role>"
    ]
}
```

# Paged data (pagination)

```json
{
    "data": [
        ...
    ],
    "page_size": 10,
    "page_number": 1,
    "total_results": 4,
    "pages": 1
}
```

Throughout the API when a large amount of data can be retrieved a standard return format is used to facilitate paging through the results.

The `data` element will contain JSON representations of the appropriate entities for your request, the other elements
are fairly self explinatory and provide sufficient information to craft navigation links.

To navigate through the pages, append a `page_number` query string parameter to your next `GET` request.